# Exercice Pratique : Introduction à WSL2

## Introduction

Cet exercice vous guidera à travers l'utilisation de Windows Subsystem for Linux 2 (WSL2) déjà installé sur votre machine. Vous apprendrez à installer une distribution Linux, à exécuter des commandes de base et à configurer un environnement de développement simple, le tout en utilisant des commandes.

## Objectifs

1. Installer une distribution Linux (Ubuntu) via les commandes.
2. Exécuter des commandes de base dans le terminal Linux.
3. Configurer un environnement de développement simple en installant Node.js et en créant une application "Hello World".

## Étapes

### 1. Installer une distribution Linux

1. Ouvrez PowerShell en mode administrateur. Pour ce faire, recherchez "Terminal" dans le menu Démarrer, faites un clic droit et sélectionnez "Exécuter en tant qu'administrateur".

2. Téléchargez la distribution Ubuntu avec la commande suivante :

    ```bash
    wsl --install ubuntu-24.04
    ```

3. Lancez Ubuntu :

    ```bash
    wsl -d ubuntu-24.04
    ```

### 2. Configurer Ubuntu

1. Lors du premier lancement, Ubuntu vous demandera de configurer un nom d'utilisateur et un mot de passe. Choisissez un nom d'utilisateur et un mot de passe sûrs.
2. Vous voilà maintenant dans le terminal Ubuntu, prêt à utiliser Linux !

### 3. Exécuter des commandes de base

Dans le terminal Ubuntu, exécutez les commandes suivantes pour vous familiariser avec l'environnement :

```bash
# Mise à jour des paquets
sudo apt update
sudo apt upgrade

# Afficher le répertoire courant
pwd

# Lister les fichiers et dossiers
ls -la

# Créer un nouveau dossier
mkdir mon_projet

# Naviguer dans le dossier
cd mon_projet

# Créer un nouveau fichier
echo "Hello World" > hello.txt

# Afficher le contenu du fichier
cat hello.txt
```

### 4. Installer Node.js et créer une application "Hello World"

#### Étape 1 : Installer Node.js

1. Exécutez les commandes suivantes pour installer Node.js et npm (le gestionnaire de paquets Node.js) :

    ```bash
    sudo apt update
    sudo apt install nodejs npm -y
    ```

2. Vérifiez l'installation en exécutant :

    ```bash
    node -v
    npm -v
    ```

#### Étape 2 : Créer une application "Hello World"

1. Dans le dossier `mon_projet`, créez un fichier `app.js` :

    ```bash
    nano app.js
    ```

2. Ajoutez le code suivant dans `app.js` :

    ```javascript
    const http = require('http');

    const hostname = '127.0.0.1';
    const port = 3000;

    const server = http.createServer((req, res) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Hello World\n');
    });

    server.listen(port, hostname, () => {
      console.log(`Server running at http://${hostname}:${port}/`);
    });
    ```

3. Enregistrez le fichier et quittez l'éditeur (Ctrl+X, puis Y et Entrée).

4. Exécutez l'application Node.js :

    ```bash
    node app.js
    ```

5. Ouvrez un navigateur web et allez à l'adresse `http://127.0.0.1:3000`. Vous devriez voir "Hello World" affiché.
