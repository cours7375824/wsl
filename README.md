# Utilisation de WSL (Windows Subsystem for Linux)

## Introduction

Ce dépôt contient la documentation, les instructions et un exercice pour configurer et utiliser le Windows Subsystem for Linux (WSL). WSL permet aux développeurs d'exécuter un environnement Linux directement sous Windows, sans avoir besoin de machines virtuelles ou de configurations dual-boot.

## Objectifs :

- Apprendre à installer et configurer WSL sur une machine Windows.
- Installer et utiliser une distribution Linux via WSL.
- Exécuter des commandes et scripts Linux directement depuis Windows.

## Contenu :

### Documentation : 
Instructions détaillées pour l'installation et la configuration de WSL, ainsi que des conseils pour une utilisation efficace.

### Tutoriel étape par étape :
Un guide complet pour installer WSL, choisir et installer une distribution Linux, et commencer à l'utiliser pour des tâches courantes.